"use strict";
var sto = window.__sto,
$ = window.jQuery,
settings = require("./../../settings.json"),
style = require("./main.css"),
html = require("./main.html"),
sto = window.__sto,
format = settings.format,
placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
container = $(html),
pos = settings.position_format,
m_txt = settings.mention_text,
index, t_link, t_m_link, page_type;

var roboto = $('<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');

$('head').append(roboto);

if (!settings.target || settings.target != "_blank") {
    t_link = "_self";
} else {
    t_link = "_blank";
}
if (!settings.mention_target || settings.mention_target != "_blank") {
    t_m_link = "_self";
} else {
    t_m_link = "_blank";
}
if (!settings.redirect || settings.redirect == "") {
    settings.redirect = "#";
}
if (!settings.mention_url || settings.mention_url == "") {
    settings.mention_url = "#";
}
//js-vignette_rayon
switch (pos) {
    case 'top':
        index = 1;
        break;
    case 'middle':
        index = 5;
        break;
    case 'bottom':
        index = 9;
        break;
    case 'ean':
        $('.l-block-search-result-grid-list .l-bloc-product-display').each(function(i,v) {
        if ($(v).find(".cl-button-quickadd-cart").attr("id") == settings.ean) {
            var place = i+1;
                index = place;
        }
        });
        break;
    default:
        index = 1;
        break;
}

module.exports = {
    init: _init_()
}

function _init_() {
    sto.load(format, function(tracker) {
        tracker.display();
        style.use();

        $(container).insertBefore($('.l-block-search-result-grid-list .l-bloc-product-display').eq(index - 1));

        if (m_txt && m_txt != "") {
            if (m_txt.length > 130) {
                m_txt = m_txt.slice(0, 130);
            }

            $(container).prepend($("<div class='sto-" + placeholder + "-legal'>" + m_txt + "</div>"));
            if(settings.mention_url != "#"){
            $('.sto-' + placeholder + '-legal').attr('type','link');
            $('.sto-' + placeholder + '-legal').on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();
                tracker.click({
                    "tl": "legal"
                });
                window.open(settings.mention_url, t_m_link);
            });
            }
        }

        if (settings.cta_img !== "") {
            // if(m_txt.length > 130){m_txt = m_txt.slice(0,130);}
            $(container).prepend($("<div class='sto-" + placeholder + "-cta'></div>"));
            $('.sto-' + placeholder + '-cta').on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();
                tracker.click();
                window.open(settings.cta_url, settings.cta_target);
            });
        }

        $('.sto-' + placeholder + '-vignette').on('click', function(e) {
            e.preventDefault();
            tracker.click({
                "tl": "redirect"
            });
            window.open(settings.redirect, t_link);
        });

        window.addEventListener("resize", function() {

            setInterval(function(){
                var v = $(".l-block-search-result-grid-list > .l-bloc-product-display[itemscope]").eq(0).outerWidth(true),
                w = $(".l-block-search-result-grid-list > .l-bloc-product-display[itemscope]").eq(0).width(),
                x = v - w,
                c = $(".l-block-search-result-grid-list").width(),
                nVignette = Math.floor(c/v); // Quantity of products into a line
                var vignetteVoid = (c - (nVignette * v)) / nVignette; //Get the void in px between each product

                $(container).css({
                    "width":w+"px",
                    "margin-right":x+"px"
                })
            }, 100);

            if(pos === "ean"){

            } else if (pos === "middle") {
                var resContainer = $(container).detach();
                $(".l-block-search-result-grid-list .l-bloc-product-display").eq(nVignette-1).after(resContainer);

            } else if(pos === "bottom"){
                var resContainer = $(container).detach();
                $(".l-block-search-result-grid-list .l-bloc-product-display").eq(nVignette*2-1).after(resContainer);
            }

        });
        window.setTimeout(function() {
            //window.dispatchEvent(new window.Event("resize"));
            var evt = window.document.createEvent('UIEvents');
            evt.initUIEvent('resize', true, false, window, 0);
            window.dispatchEvent(evt);
        });

        //TRACKING VIEW
        var trackingView = false;
        var windowCenterTop = $(window).height() / 4;
        var windowCenterBot = $(window).height() - windowCenterTop;
        var eTop = $('.sto-' + placeholder + '-vignette').offset().top;
        var distanceToTop;
        var trackScroll = function() {
            distanceToTop = eTop - $(window).scrollTop();
            if (distanceToTop >= windowCenterTop && distanceToTop <= windowCenterBot && trackingView == false) {
                tracker.view();
                trackingView = true;
            }
        };
        trackScroll();
        $(window).scroll(function() {
            trackScroll();
        });

        var removers = {};
        removers[format] = function() {
            style.unuse();
            container.remove();
        };
        return removers;
    });
}

"use strict";
var settings = require("./../../settings.json"),
    style = require("./main.css"),
    html = require("./main.html"),
    sto = window.__sto,
    format = settings.format,
    placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
    container = $(html),
    pos = settings.position_format === "top" ? "1" : settings.position_format === "middle" ? "5" : settings.position_format === "bottom" ? "9" : "1",
    m_txt = settings.mention_text,
    modulo = pos % 4,
    index, ligne, t_link, t_m_link;

if (!settings.target || settings.target != "_blank") {
    t_link = "_self";
} else {
    t_link = "_blank";
}
if (!settings.mention_target || settings.mention_target != "_blank") {
    t_m_link = "_self";
} else {
    t_m_link = "_blank";
}

module.exports = {
    init: _init_(),
    styleGutter: style
}

function _init_() {
    if (modulo == 0) {
        index = 3;
        ligne = parseInt(pos / 4) - 1;
    } else {
        index = modulo - 1;
        ligne = parseInt(pos / 4);
    }
    sto.load(format, function(tracker) {
        tracker.display();
        style.use();
        container.closest(".sto-" + placeholder + "-vignette").attr('data-index', index);

        if ($('.resultats-recherche').length != 0) {
            container.closest(".sto-" + placeholder + "-vignette").attr("data-page", "search");
        } else {
            container.closest(".sto-" + placeholder + "-vignette").attr("data-page", "rayon")
        }

        if ($("#crossZone-0").length > 0) {
            var nbRow = $("#central-container .row").length,
                target;

            if (getNbArticle() >= 4) {
                target = $($("#itemsList .row")[ligne]).find(".vignette-box").eq(index);
                $(container).insertBefore(target);
            } else {
                ligne = false;
            }

            if (ligne || ligne == 0) {
                for (ligne; ligne <= nbRow; ligne++) {
                    if ($('#central-container .row' + (ligne + 1) + ' .vignette-box').length == 5) {

                        var i = $('#central-container .row' + (ligne + 1));
                        var s = i.find('.vignette-box').eq(4);

                        var j = $('#central-container .row' + (ligne + 2));
                        var t = j.find('.vignette-box').eq(0);

                        if (t) {
                            s.insertBefore(t);
                        } else if (i) {
                            j.prepend(s)
                        }
                    }
                }
            }
        } else {
            if (getNbArticle() >= 4) {
                e = parseInt(pos - 1);
                target = $($("#itemsList .vignette-box")[e]);
                $(container).insertBefore(target);
            }
        }

        if (m_txt && m_txt != "") {
            if (m_txt.length > 130) {
                m_txt = m_txt.slice(0, 130);
            }
            $(container).prepend($("<div class='sto-" + placeholder + "-cta'>" + m_txt + "</div>"));
        }

        if (settings.cta_img !== "") {
            // if(m_txt.length > 130){m_txt = m_txt.slice(0,130);}
            $(container).prepend($("<div class='sto-" + placeholder + "-button'></div>"));
        }

        $('.sto-' + placeholder + '-cta').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            tracker.click({
                "tl": "legal"
            });
            window.open(settings.mention_url, t_m_link);
        });
        $('.sto-' + placeholder + '-button').on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            tracker.click();
            window.open(settings.cta_url, settings.cta_target);
        });

        $('.sto-' + placeholder + '-vignette').on('click', function(e) {
            e.preventDefault();
            tracker.click({
                "tl": "redirect"
            });
            window.open(settings.redirect, t_link);
        });

        //TRACKING VIEW
        var trackingView = false;
        var windowCenter = $(window).height() / 3;
        var eTop = $('.sto-' + placeholder + '-vignette').offset().top;
        var distanceToTop;
        var trackScroll = function() {
            distanceToTop = eTop - $(window).scrollTop();
            if (distanceToTop >= 100 && distanceToTop <= 700 && trackingView == false) {
                tracker.view();
                trackingView = true;
            }
        };
        trackScroll();
        $(window).scroll(function() {
            trackScroll();
        });

        setTimeout(function(){
              $(window).on('scroll', function() {
                  //var pageProdsQty = setInterval(function(){
                  //if (($('#central-container .vignette').length - $('.sto-' + placeholder + '-bundleboost .vignette').length) > 20) {
                      for (var i = 0; i < $('#central-container .row').length; i++) {
                          if ($('#central-container .row').eq(i).children('.vignette-box').length > 4) {
                              if ($('#central-container .row').eq(i + 1)) {
                                  $('#central-container .row').eq(i + 1).prepend($('#central-container .row').eq(i).children('.vignette-box').eq(4));
                              }
                          }
                      }
                  //}
                  //}, 300);
              });
            }, 600);







        var removers = {};
        removers[format] = function() {
            style.unuse();
            container.remove();
        };
        return removers;
    });
}

function getNbArticle() {
    var nbProducts = $(".vignette-box").length;
    if (nbProducts)
        return nbProducts;
    else
        return false;
}

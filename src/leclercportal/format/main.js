"use strict";
var sto = window.__sto,
  jQuery = window.jQuery,
  settings = require("./../../settings.json"),
  style = require("./main.css"),
  html = require("./main.html"),
  format = settings.format,
  placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
  myurl = window.location.href,
  container = jQuery(html);

module.exports = {
  init: _init_()
}

function _init_() {

  sto.load(format, function(tracker) {

    style.use();

    /* ---------------------------------
        Gère l'affichage du format
       --------------------------------- */

    // Paramètres pour l'affiche sur la page promotion
    if (myurl === "http://www.e-leclerc.com/catalogue/promotions") {

      var intPromo = setInterval(function(){
        if (jQuery('.box-item-rayon').length > 0) {

          jQuery(container)
          .attr("data-page","promo") // Ajoute un attribut pour gérer les différents CSS
          .insertAfter('.list-products-content .col-sm-third:nth-child(2)');

          window.addEventListener("resize", function() {

            // Modifie la position du format par rapport aux autres contenus
            if ($(window).width() < 1006 && $(window).width() > 750 ) {

              jQuery(container)
                .detach()
                .insertAfter('.list-products-content .col-sm-third:nth-child(1)')
                .width($(".col-xs-16 .row .col-xs-16:first-child").width() * 2 + 17);

            } else if ($(window).width() < 750) {

              jQuery(container)
                .detach()
                .insertAfter('.list-products-content .col-sm-third:nth-child(2)')
                .width($(".col-xs-16 .row .col-xs-16:first-child").width() - 10);

            } else { // Repositionne le format

              jQuery(container)
                .detach()
                .insertAfter('.list-products-content .col-sm-third:nth-child(2)')
                .width($(".col-xs-16 .row .col-xs-16:first-child").width() * 2 + 17);
            }
          });

          clearInterval(intPromo);
        }
      }, 500);

    } else {  // Paramètres pour l'affiche sur les pages rayon

      jQuery(container)
        .attr("data-page","rayon") // Ajoute un attribut pour gérer les différents CSS
        .insertAfter('body .page-container .middle .content-right .row section .owl-wrapper-outer .owl-wrapper .owl-item:nth-child(2)');

      window.addEventListener("resize", function() {

        // Modifie la position du format par rapport aux autres contenus
        if ($(window).width() < 1256) {

          jQuery(container)
            .detach()
            .insertAfter('body .page-container .middle .content-right .row section .owl-wrapper-outer .owl-wrapper .owl-item:nth-child(1)');

        } else { // Repositionne le format

          $(".owl-prev").css("left", "-18px");
          $(".owl-next").css("right", "-16px");

          jQuery(container)
            .detach()
            .insertAfter('body .page-container .middle .content-right .row section .owl-wrapper-outer .owl-wrapper .owl-item:nth-child(2)');
        }
      });
    }

    /* ---------------------------------
        Redirection Tracking
       --------------------------------- */


    setTimeout(function(){
      if ($(".sto-" + placeholder + "-container").length > 0) {
        tracker.display();
       }
    }, 500);

    var checkView = false;
    console.log("checkView", checkView);

    if (!checkView) {
            setTimeout(function() {
                if ($(".sto-" + placeholder + "-container").length > 0 && !checkView) {
                  console.log("tracker 1");
                  console.log($($(".sto-" + placeholder + "-container")));
                    if ($($(".sto-" + placeholder + "-container"))[0].getBoundingClientRect().top >= 0 && $($(".sto-" + placeholder + "-container"))[0].getBoundingClientRect().top <= 654) {
                        tracker.view();
                        checkView = true;
                    }
                }
            }, 500);
            $(window).scroll(function() {
                if ($(".sto-" + placeholder + "-container").length > 0) {
                    if ($($(".sto-" + placeholder + "-container"))[0].getBoundingClientRect().top >= 0 && $($(".sto-" + placeholder + "-container"))[0].getBoundingClientRect().top <= 654 && !checkView && $($(".sto-" + placeholder + "-container")[0]).length > 0) {
                        tracker.view();
                        checkView = true;
                    }
                }
            });
        }

    jQuery('body').on('click', '.sto-' + placeholder + '-container', function() {
      tracker.click();
      window.open(settings.redirect, settings.target);
    });


    var bannerResponsiveW;
    window.addEventListener("resize", function() {
      bannerResponsiveW = jQuery('.sto-' + placeholder + '-container').width();
      jQuery('.sto-' + placeholder + '-container').css('height', ((25.7732 * bannerResponsiveW) / 100) + 'px');
    });
    window.setTimeout(function() {
      //window.dispatchEvent(new window.Event("resize"));
      var evt = window.document.createEvent('UIEvents');
      evt.initUIEvent('resize', true, false, window, 0);
      window.dispatchEvent(evt);
    });


    var removers = {};
    removers[format] = function() {
      style.unuse();
      container.remove();
    };
    return removers;
  });
}

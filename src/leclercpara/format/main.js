"use strict";
var settings = require("./../../settings.json"),
    style = require("./main.css"),
    html = require("./main.html"),
    sto = window.__sto,
    jQuery = window.jQuery,
    $ = jQuery,
    format = settings.format,
    placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
    container = jQuery(html),
    pos = settings.position_format,
    m_txt = settings.mention_text,
    nbImg = settings.img_number,
    nbImgMobile = settings.img_number_mobile,
    myurl = window.location.href;


module.exports = {
    init: _init_()
}

function _init_() {
    sto.load(format, function(tracker) {

        style.use();

        /* ---------------------------------
             Gère l'affichage du format
           --------------------------------- */


        console.log(jQuery(window).width());

        var displayKikers = function (numberImg, target) {

            if (numberImg === "1") {

                jQuery(target).prepend(container);

                if (jQuery(".sto-" + placeholder + "-secondVignette").length > 0) {

                    jQuery(".sto-" + placeholder + "-secondVignette").hide();

                }

            } else if (numberImg === "2") {

                jQuery(target).prepend(container);

                if (jQuery(".sto-" + placeholder + "-secondVignette").length == 0) {

                    jQuery(".sto-" + placeholder + "-container").append("<div class='sto-" + placeholder + "-secondVignette'></div>");

                } else if (jQuery(".sto-" + placeholder + "-secondVignette").length > 0) {

                    jQuery(".sto-" + placeholder + "-secondVignette").show();

                }
            } else {

                jQuery(".sto-" + placeholder + "-container").remove();

            }
        }


        window.addEventListener("resize", function() {

            if (jQuery(window).width() > 973) {

                displayKikers(nbImg, "#container-kicker .container-center .produit_home_123 div:first-child a");

            } else if (jQuery(window).width() < 973 && jQuery(window).width() > 754) {

                displayKikers(nbImg, "#onglet");

            } else if (jQuery(window).width() < 754) {

                displayKikers(nbImgMobile, "#onglet");
            }
        });


        /* ---------------------------------
             Redirection Tracking
           --------------------------------- */

        tracker.display();
        tracker.view();

        jQuery('body').on('click', '.sto-' + placeholder + '-vignette', function() {

            window.open(settings.redirect, settings.target);

            tracker.click();

        });

        jQuery('body').on('click', '.sto-' + placeholder + '-secondVignette', function() {

            window.open(settings.redirect2, settings.target);

            tracker.click();

        });


        var bannerResponsiveW;
        window.addEventListener("resize", function() {
          bannerResponsiveW = jQuery('.sto-' + placeholder + '-container').width();
          jQuery('.sto-' + placeholder + '-container').css('height', ((25.7732 * bannerResponsiveW) / 100) + 'px');
        });
        window.setTimeout(function() {
          var evt = window.document.createEvent('UIEvents');
          evt.initUIEvent('resize', true, false, window, 0);
          window.dispatchEvent(evt);
        });


        var removers = {};
        removers[format] = function() {
          style.unuse();
          container.remove();
        };
        return removers;

    });
}

var settings = require("./settings.json");
settings.bg_img_format = settings.bg_img_format === "" ? "none" : "url(./../../img/"+settings.bg_img_format+")";
settings.bg_img_format_second = settings.bg_img_format_second === "" ? "none" : "url(./../../img/"+settings.bg_img_format_second+")";
settings.bg_img_format_mobile = settings.bg_img_format_mobile === "" ? "none" : "url(./../../img/"+settings.bg_img_format_mobile+")";
settings.bg_img_format_mobile_second = settings.bg_img_format_mobile_second === "" ? "none" : "url(./../../img/"+settings.bg_img_format_mobile_second+")";

settings.cta_bg_couleur = settings.cta_bg_couleur === "" ? "transparent" : settings.cta_bg_couleur;
settings.cta_hover_bg_couleur = settings.cta_hover_bg_couleur === "" ? "transparent" : settings.cta_hover_bg_couleur;
settings.cta_text_couleur = settings.cta_text_couleur === "" ? "transparent" : settings.cta_text_couleur;
settings.cta_hover_text_couleur = settings.cta_hover_text_couleur === "" ? "transparent" : settings.cta_hover_text_couleur;

settings.cta_img = settings.cta_img === "" ? "none" : "url(./../../img/"+settings.cta_img+")";
settings.cta_img_hover = settings.cta_img_hover === "" ? "none" : "url(./../../img/"+settings.cta_img_hover+")";
settings.bg_couleur_format = settings.bg_couleur_format === "" ? "none" : settings.bg_couleur_format;
settings.mention_color = settings.mention_color === "" ? "#000" : settings.mention_color;
settings.bg_couleur_mention = settings.bg_couleur_mention === "" ? "transparent" : settings.bg_couleur_mention;
settings.left_bg_color = settings.left_bg_color === "" ? "transparent" : settings.left_bg_color;
settings.left_text_color = settings.left_text_color === "" ? "" : settings.left_text_color;
settings.left_border_color = settings.left_border_color === "" ? "transparent" : settings.left_border_color;
settings.left_cat_bg_color = settings.left_cat_bg_color === "" ? "transparent" : settings.left_cat_bg_color;
settings.left_cta_hover_bg_color = settings.left_cta_hover_bg_color === "" ? "transparent" : settings.left_cta_hover_bg_color;
settings.left_cta_text_color = settings.left_cta_text_color === "" ? "transparent" : settings.left_cta_text_color;
settings.left_cta_hover_text_color = settings.left_cta_hover_text_color === "" ? "transparent" : settings.left_cta_hover_text_color;

module.exports = function (source) {
    this.cacheable();
    var test = source
        .replace(/__PLACEHOLDER__/g, settings.format+"_"+settings.name+"_"+settings.creaid)
        .replace(/__FORMAT__/g, settings.format)
        .replace(/__BGIMGFORMAT__/g,settings.bg_img_format)
        .replace(/__BGIMGFORMAT_SECOND__/g,settings.bg_img_format_second)
        .replace(/__BGIMGFORMAT_MOBILE__/g,settings.bg_img_format_mobile)
        .replace(/__BGIMGFORMAT_MOBILE_SECOND__/g,settings.bg_img_format_mobile_second)
        .replace(/__CTA_BG_COULEUR__/g,settings.cta_bg_couleur)
        .replace(/__CTA_HOVER_BG_COULEUR__/g,settings.cta_hover_bg_couleur)
        .replace(/__CTA_TEXT_COULEUR__/g,settings.cta_text_couleur)
        .replace(/__CTA_HOVER_TEXT_COULEUR__/g,settings.cta_hover_text_couleur)
        .replace(/__CTA_IMG__/g,settings.cta_img)
        .replace(/__CTA_IMG_HOVER__/g,settings.cta_img_hover)
        .replace(/__BGCOULEURFORMAT__/g,settings.bg_couleur_format)
        .replace(/__MENTION_COLOR__/g,settings.mention_color)
        .replace(/__BGCOULEURMENTION__/g,settings.bg_couleur_mention)
        .replace(/__LEFT_BG_COULEUR__/g,settings.left_bg_color)
        .replace(/__LEFT_TEXT_COULEUR__/g,settings.left_text_color)
        .replace(/__LEFT_CTA_BGCOULEUR__/g,settings.left_cat_bg_color)
        .replace(/__LEFT_CTA_HOVER_BGCOULEUR__/g,settings.left_cta_hover_bg_color)
        .replace(/__LEFT_CTA_TEXT_COULEUR__/g,settings.left_cta_text_color)
        .replace(/__LEFT_CTA_HOVER_TEXT_COULEUR__/g,settings.left_cta_hover_text_color)
        .replace(/__LEFT_BORDER_COULEUR__/g,settings.left_border_color);
    return test;
};

"use strict";
var settings = require("./../../settings.json"),
    style = require("./main.css"),
    html = require("./main.html"),
    sto = window.__sto,
    format = settings.format,
    placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
    container = $(html),
    pos = settings.position_format,
    m_txt = settings.mention_text,
    myurl = window.location.href.replace("https://","").replace("http://","").replace("//","").replace("www.coradrive.fr/", ""),
    mydrive = myurl.split("/")[0],
    mention_url = settings.mention_url,
    redirect_url = settings.redirect,
    myurl = myurl.replace("https://", "").split("/")[0],
    index, t_link, t_m_link, page_type;
    //url_last_redirect = redirect_url.indexOf('https://www.coradrive.fr') >= 0 ? redirect_url.replace(redirect_url.replace("https://www.coradrive.fr/", "").split("/")[0],myurl) : redirect_url;

if (redirect_url.indexOf('www.coradrive.fr') >= 0) {
    redirect_url = redirect_url.replace("https://","").replace("http://","").replace("//","").replace("www.coradrive.fr/", "");
    var url_redirection = "/" + mydrive,
        y = 1;
    for (y; y < redirect_url.split("/").length; y++) {
        url_redirection = url_redirection + "/" + redirect_url.split("/")[y]
    }
} else {
    var url_redirection = redirect_url;
}

if (mention_url.indexOf('www.coradrive.fr') >= 0) {
    mention_url = mention_url.replace("https://","").replace("http://","").replace("//","").replace("www.coradrive.fr/", "");
    var url_mention = "/" + mydrive,
        y = 1;
    for (y; y < mention_url.split("/").length; y++) {
        url_mention = url_mention + "/" + mention_url.split("/")[y]
    }
}  else {
    var url_mention = mention_url;
}

var roboto = $('<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');
$('head').append(roboto);

page_type = $(document).find('#dataRecherche').length != 0 ? "recherche" : "rayon";
t_link = settings.target == "_blank" ? "_blank" : "_self";
t_m_link = settings.mention_target == "_blank" ? "_blank" : "_self";


if (!settings.redirect || settings.redirect == "") {
    settings.redirect = "";
}
if (!settings.mention_url || settings.mention_url == "") {
    settings.mention_url = "";
}
//js-vignette_rayon
switch (pos) {
    case 'top':
        index = 1;
        break;
    case 'middle':
        if (page_type === "rayon") {index = 6;}
        else {index = 5;}
        break;
    case 'bottom':
        if (page_type === "rayon") {index = 10;}
        else {index = 9;}
        break;
    default:
        index = 1;
        break;
}

module.exports = {
    init: _init_()
}

function _init_() {
    sto.load(format, function(tracker) {
        tracker.display();
        style.use();
        $(container).insertBefore($('.wrapinner #container .grille div.col1').eq(index - 1));

      $(container).attr('data-page', page_type);
        if (m_txt && m_txt != "") {
            if (m_txt.length > 130) {
                m_txt = m_txt.slice(0, 130);
            }
            $(container).prepend($("<div class='sto-" + placeholder + "-legal'>" + m_txt + "</div>"));
            if (settings.mention_url != "#") {
                $('.sto-' + placeholder + '-legal').attr('type', 'link');
                $('.sto-' + placeholder + '-legal').on('click', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    tracker.click({
                        "tl": "legal"
                    });
                    window.open(url_mention, t_m_link);
                });
            }
        }
        $('.sto-' + placeholder + '-vignette').on('click', function(e) {
            e.preventDefault();
            tracker.click({
                "tl": "redirect"
            });
            window.open(url_redirection, t_link);
        });

        //TRACKING VIEW
        var trackingView = false;
        var windowCenterTop = $(window).height() / 4;
        var windowCenterBot = $(window).height() - windowCenterTop;
        var eTop = $('.sto-' + placeholder + '-vignette').offset().top;
        var distanceToTop;
        var trackScroll = function() {
            distanceToTop = eTop - $(window).scrollTop();
            if (distanceToTop >= windowCenterTop && distanceToTop <= windowCenterBot && trackingView == false) {
                tracker.view();
                trackingView = true;
            }
        };
        trackScroll();
        $(window).scroll(function() {
            trackScroll();
        });


        setTimeout(function(){
            $('body')
            .on('.sto-' + placeholder + '-vignette','click', function(e) {
                e.preventDefault();
                tracker.click({
                    "tl": "redirect"
                });
                window.open(url_redirection, t_link);
            });
        },500);

        var removers = {};
        removers[format] = function() {
            style.unuse();
            container.remove();
        };
        return removers;

    });
}

function getNbArticle() {
    var nbProducts = $(".vignette-box").length;
    if (nbProducts)
        return nbProducts;
    else
        return false;
}

"use strict";
var settings = require("./../../settings.json"),
  style = require("./main.css"),
  html = require("./main.html"),
  sto = window.__sto,
  format = settings.format,
  placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
  creaid = settings.creaid,
  container = $(html),
  target = settings.cta_target != "_blank" && settings.target != "_self" ? "_self" : settings.target,
  m_target = settings.mention_target != "_blank" && settings.mention_target != "_self" ? "_self" : settings.mention_target,
  pos,
  m_txt = settings.mention_text,
  cta_txt = settings.cta_text,
  left_txt = settings.left_text,
  left_cta = settings.left_cta_text,
  // left_cta_redirect = settings.left_cta_redirect != "_blank" && settings.left_cta_redirect != "_self" ? "_self" : settings.left_cta_redirect,
  myurl = window.location.href,
  myurl = myurl.replace("https://", "").split("/")[1],
  big_line, line_half;

var roboto = $('<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');
$('head').append(roboto);

module.exports = {
  init: _init_()
}

function _init_() {

  if (window.__sto.getTrackers("VI").constructor === Array) {
    var cosa = [format, creaid];
  } else {
    var cosa = format;
  }






  sto.load([format, creaid], function(tracker) {

    var removers = {};
    removers[format] = {
      "creaid": creaid,
      "func": function() {
        style.unuse();
        container.remove(); //verifier containerFormat j'ai fait un copier coller
      }
    };


    var int, isElementInViewport;

    isElementInViewport = function(el) {

      if (typeof window.jQuery === "function" && el instanceof window.jQuery) {
        el = el[0];
      }

      var rect = el.getBoundingClientRect();

      return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        !(rect.top == rect.bottom || rect.left == rect.right) &&
        !(rect.height == 0 || rect.width == 0) &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
        rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
      );

    }



    // if (window.matchMedia('(min-width: 786px)').matches) {
    //   tracker.error({
    //     "tl": "onlyOnMobile"
    //   });
    //   return removers;
    //   return false;
    // }



    /*if ($('div[data-pos="' + settings.position_format + '"]').length > 0) {
      tracker.error({
        "tl": "placementOcuppied"
      });
      return removers;
      return false;
    }*/

    if ($('div[data-pos="13"]').length > 0 && $('div[data-pos="25"]').length > 0) {
      tracker.error({
        "tl": "placementOcuppied"
      });
      return removers;
      return false;
    }


    // var prodsInpage = $('#productList>li.product').length;
    //
    // if (prodsInpage >= 14 && prodsInpage <= 20 && $('.sto-' + format).length < 1) {
    //
    // } else if (prodsInpage > 20 && $('.sto-' + format).length < 2) {
    //
    // } else {
    //   tracker.error({
    //     "tl": "notEnoughProdsInPage"
    //   });
    //   return removers;
    //   return false;
    // }

    style.use();



    // container.attr('data-pos', settings.position_format);
    container.attr('data-format-type', settings.format);
    container.attr('data-crea-id', settings.creaid);


    /*$('#productList>li.product').eq(parseInt(settings.position_format) - 1).before(container);*/


    // <editor-fold> INSERT CONTAINER *****************************************************
    // var insertContainer = function() {


    //   switch (settings.position_format) {
    //     case '13':
    //           // if (window.matchMedia("(max-width: 767px)").matches) {
    //           //   $('#productList>li.pl-product').eq(12).before(container);
    //           // } else {
    //             $('#productList>li.pl-product').eq(12).before(container);
    //           // }
    //         break;
    //     case '25':
    //           // if (window.matchMedia("(max-width: 767px)").matches) {
    //           //   $('#productList>li.pl-product').eq(24).before(container);
    //           // } else {
    //             $('#productList>li.pl-product').eq(24).before(container);
    //           // }
    //       break;
    //   }
    // }
    //
    // insertContainer();

    //insert format
          var insertFormat = function() {

            var widthTile = $('#productList>li').width();
            var widthLine = $('.product-list').width();
            var nbTile;

            if (widthTile * 2 > widthLine) {
              nbTile = 1;
            } else if (widthTile * 3 > widthLine) {
              nbTile = 2;
            } else {
              nbTile = 3;
            }

            if ($('.sto-format').length == 0) {
              $('#productList>li').eq( nbTile * 5 ).before(container);
              $('.sto-format:first-of-type').attr('data-pos', '6');
            } else if ($('.sto-format').length == 1) {
              $('#productList>li').eq( nbTile * 15 ).before(container);
              $('.sto-format:nth-of-type(2)').attr('data-pos', '16');
            } else if ($('.sto-format').length == 2) {
              $('#productList>li').eq( nbTile * 25 ).before(container);
              $('.sto-format:nth-of-type(3)').attr('data-pos', '26');
            }
          }

          insertFormat();



    // </editor-fold> *********************************************************************



    // <editor-fold> LEFT *****************************************************************

    // TEXTE
    if (left_txt && left_txt != "") {
      $(container).find('.sto-' + placeholder + '-vignette-left .sto-' + placeholder + '-left-content').prepend($("<p class='sto-" + placeholder + "-left-txt'>" + left_txt + "</p>"));
    }

    // CTA
    if (left_cta && left_cta != "") {
      if (left_cta.length > 130) {
        left_cta = left_cta.slice(0, 130);
      }
      $(container).find('.sto-' + placeholder + '-vignette-left .sto-' + placeholder + '-left-content').append($("<a class='sto-" + placeholder + "-left-cta'>" + left_cta + "</a>"));
      if (settings.left_cta_redirect != "") {
        $('.sto-' + placeholder + '-left-cta').attr('type', 'link');
        $('.sto-' + placeholder + '-left-cta').on('click', function(e) {
          e.preventDefault();
          e.stopPropagation();
          tracker.click({
            "tl": "CTA",
            "po": pos
          });
          window.open(settings.left_cta_redirect);
        });
      }
    }



    // </editor-fold> *********************************************************************



    // <editor-fold> RESIZE ***************************************************************
    $(window).resize(function () {

      var resizeFormat = function() {
        var widthTile = $('#productList>li').width();
        var widthLine = $('.product-list').width();
        var nbTile;

        if (widthTile * 2 > widthLine) {
          nbTile = 1;
        } else if (widthTile * 3 > widthLine) {
          nbTile = 2;
        } else {
          nbTile = 3;
        }

        $('.sto-format').each(function() {
          var dataPos = $(this).attr('data-pos');
          $('#productList>li').eq( nbTile * (dataPos - 1)).before(this);
        });
      }
      resizeFormat();

    });


    // </editor-fold> *********************************************************************




    tracker.display({
      "po": pos
    });

    //VIEW
    if (settings.type == "buttons") {
      int = window.setInterval(function() {

        if (isElementInViewport(container)) {
          tracker.view({
            "po": pos
          });
          window.clearInterval(int);
        }

      }, 200);
    }


    // <editor-fold> TEXTE VIGNETTE *******************************************************
    if (m_txt && m_txt != "") {
      if (m_txt.length > 130) {
        m_txt = m_txt.slice(0, 130);
      }
      $(container).find('.sto-' + placeholder + '-vignette').prepend($("<div class='sto-" + placeholder + "-legal'><span>" + m_txt + "</span></div>"));
    }
    // </editor-fold> *********************************************************************


    // <editor-fold> CTA VIGNETTE *********************************************************
    // if (cta_txt && cta_txt != "") {
    //   if (cta_txt.length > 130) {
    //     cta_txt = cta_txt.slice(0, 130);
    //   }
    //   $(container).find('.sto-' + placeholder + '-vignette').prepend($("<div class='sto-" + placeholder + "-cta'><span>" + cta_txt + "</span></div>"));
    //   if (settings.cta_url != "") {
    //     $('.sto-' + placeholder + '-cta').attr('type', 'link');
    //     $('.sto-' + placeholder + '-cta').on('click', function(e) {
    //       e.preventDefault();
    //       e.stopPropagation();
    //       tracker.click({
    //         "tl": "legal",
    //         "po": pos
    //       });
    //       window.open(settings.cta_url, target);
    //     });
    //   }
    // }
    // </editor-fold> *********************************************************************



    // $('.sto-' + placeholder + '-vignette>a').attr('href',settings.redirect);
    // $('.sto-' + placeholder + '-vignette>a').attr('target',settings.target);
    // $('.sto-' + placeholder + '-vignette>a').on('click', function (e) {
      // tracker.click({
      //   "tl": "redirect",
      //   "po": pos
      // });
      // window.setTimeout(function(){
      //   /*if (settings.target == '_self') {
      //     window.location.href = settings.redirect;
      //   }*/
      //
      // }, 200)
      if (settings.redirect != "") {
        $('.sto-' + format + '>a').attr('href',settings.redirect);
        $('.sto-' + format + '>a').attr('target',settings.target);
        $('.sto-' + format + '>a').on('click', function (e) {

          e.preventDefault();
          e.stopPropagation();
          tracker.click({
            "tl": "redirect",
            "po": pos
          });
          window.open(settings.redirect, settings.target);
        });
      }


    //TRACKING VIEW
    var trackingView = false;
    var windowCenterTop = $(window).height() / 4;
    var windowCenterBot = $(window).height() - windowCenterTop;
    var eTop = $('.sto-' + placeholder + '-vignette').offset().top;
    var distanceToTop;
    var trackScroll = function() {
      distanceToTop = eTop - $(window).scrollTop();
      if (distanceToTop >= windowCenterTop && distanceToTop <= windowCenterBot && trackingView == false) {
        tracker.sendHit({
          "ta": "view",
          "po": pos
        });
        trackingView = true;
      }
    };
    trackScroll();
    $(window).scroll(function() {
      trackScroll();
    });


    window.addEventListener("resize", function() {

        if (window.matchMedia("(max-width: 767px)").matches) {
          $('.sto-' + format).css('height', ((77.3 * $('.sto-' + placeholder + '-vignette').width()) / 100) + 'px');
        }

    });
    window.setTimeout(function() {
      //window.dispatchEvent(new window.Event("resize"));
      var evt = window.document.createEvent('UIEvents');
      evt.initUIEvent('resize', true, false, window, 0);
      window.dispatchEvent(evt);
    });

    //STORE FORMAT FOR FILTERS
    sto.globals["print_creatives"].push({
      html: $('.sto-' + placeholder + '-vignette'),
      parent_container_id: "#productList",
      type: settings.format,
      crea_id: settings.creaid,
      position: pos
    });

    return removers;
  });
}

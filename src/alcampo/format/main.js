"use strict";
var settings = require("./../../settings.json"),
    style = require("./main.css"),
    html = require("./main.html"),
    sto = window.__sto,
    format = settings.format,
    placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
    container = $(html),
    pos = settings.position_format,
    m_txt = settings.mention_text,
    cta_img = settings.cta_img,
    myurl = window.location.href,
    myurl = myurl.replace("https://", "").split("/")[1],
    index, t_link, t_m_link, page_type;


// google font
var roboto = $('<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');
$('head').append(roboto);

// if (window.location.href.indexOf('recherche') > -1) {
//     page_type = "recherche"
// } else {
//     page_type = "rayon"
// }

if (!settings.target || settings.target != "_blank") {
    t_link = "_self";
} else {
    t_link = "_blank";
}
if (!settings.mention_target || settings.mention_target != "_blank") {
    t_m_link = "_self";
} else {
    t_m_link = "_blank";
}
if (!settings.redirect || settings.redirect == "") {
    settings.redirect = "";
}
if (!settings.mention_url || settings.mention_url == "") {
    settings.mention_url = "";
}
//js-vignette_rayon
// switch (pos) {
//     case 'top':
//         index = 1;
//         break;
//     case 'mid':
//         index = 5;
//         break;
//     case 'bot':
//         index = 9;
//         break;
//     default:
//         index = 1;
//         break;
// }

module.exports = {
    init: _init_()
}




function _init_() {
    sto.load(format, function(tracker) {

        tracker.display();
        style.use();


        // var emplacement = $(".productGrid .fila4 .span-5").eq(index - 1);
        // $(container).insertBefore(emplacement);
        // $(".productGrid .fila4 .span-5").eq(0).before(container);

      var position = function() {
        switch (pos) {
            case 'top':

                $('.fila4').eq(0).prepend(container);
                break;

            case 'mid':

                $('.fila4').eq(1).prepend(container);
                break;

            // case 'bot':
            //
            //     $('.fila4').eq(2).prepend(container);
            //     break;

            default:

                $('.fila4').eq(0).prepend(container);
                break;
          }
      };

      position();



        // $('.fila4').eq(0).prepend('<div class="sto-empty-div"></div>');
        $('.productGrid').eq(0).append('<div class="fila4"></div>');


                    for (var i = 0; i <= $('.fila4').length; i++) {
                        if ($('.fila4').eq(i).children('div').length > 4) {
                            for (var j = 0; j < 1; j++) {
                                var curProd = $('.fila4').eq(i).children('div').eq(3);

                                curProd = curProd.attr('signal', 'coucou');
                                if ($('.fila4').eq(i + 1).length > 0) {
                                    $('.fila4').eq(i + 1).prepend(curProd);
                                } else {
                                    $('.fila4').eq(i).after(curProd);
                                }

                            }
                        }
                        $('.fila4').eq(i).children('div.span-5').attr('class', 'span-5');
                        $('.fila4').eq(i).children('div').first().addClass('first clear');
                        $('.fila4').eq(i).children('div').last().addClass('last');
                    }




        // if (pos == "mid") {
        //     $('.sto-' + placeholder + '-vignette').attr('pos', 'mid')
        // }
        // if (pos == "bot") {
        //     $('.sto-' + placeholder + '-vignette').attr('pos', 'bot')
        // }


        $(container).attr('data-page', page_type).addClass('span-5');

        // largeur mode tablette
        var largeurTablette = function () {

          if ($('body').hasClass('tablet')) {
            $('.sto-' + placeholder + '-vignette').addClass('tablette');
          } else {
            $('.sto-' + placeholder + '-vignette').removeClass('tablette');
          }
        }


        // height vignette
        var vignetteHeight = $('.span-5').eq(1).height() - 10;
        $('.sto-' + placeholder + '-vignette').css('height', vignetteHeight + 'px');



        if (cta_img || m_txt != "") {
            if (m_txt.length > 130) {
                m_txt = m_txt.slice(0, 130);
            }
            $(container).prepend($("<div class='sto-" + placeholder + "-legal'>" + m_txt + "</div>"));
            if (settings.mention_url != "#") {
                $('.sto-' + placeholder + '-legal').attr('type', 'link');
                $('.sto-' + placeholder + '-legal').on('click', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    tracker.click({
                        "tl": "legal"
                    });
                    window.open(settings.mention_url.replace("_DRIVE_", myurl), t_m_link);
                });
            }
        }

        $('.sto-' + placeholder + '-vignette').on('click', function(e) {
            e.preventDefault();
            tracker.click({
                "tl": "redirect"
            });
            window.open(settings.redirect.replace("_DRIVE_", myurl), t_link);
        });

        //TRACKING VIEW
        var trackingView = false;
        var windowCenterTop = $(window).height() / 4;
        var windowCenterBot = $(window).height() - windowCenterTop;
        var eTop = $('.sto-' + placeholder + '-vignette').offset().top;
        var distanceToTop;
        var trackScroll = function() {
            distanceToTop = eTop - $(window).scrollTop();
            if (distanceToTop >= windowCenterTop && distanceToTop <= windowCenterBot && trackingView == false) {
                tracker.view();
                trackingView = true;
            }
        };
        trackScroll();
        $(window).scroll(function() {
            trackScroll();
        });




        // change position responsive
        var tgSize = 0;
        var winSize;

        var reposition = function() {
            winSize = $(window).width();
            //console.log(winSize);
            if (pos === "mid") {
              if (winSize <= 719) {
                $('.sto-' + placeholder + '-vignette').detach();
                $('.fila4').eq(0).children('.span-5').eq(1).after(container);

                $('.fila4').eq(0).children('.span-5').eq(4).detach().insertBefore('.fila4:nth-of-type(2) .span-5:first-of-type');
              } else {
                $('.sto-' + placeholder + '-vignette').detach();
                $('.fila4').eq(1).children('.span-5').eq(0).before(container);

                for (var i = 0; i <= $('.fila4').length; i++) {
                    if ($('.fila4').eq(i).children('div').length > 4) {
                            var curProd = $('.fila4').eq(i).children('div').eq(1);

                            $('.fila4').eq(i - 1).children('.span-5').eq(2).after(curProd);

                    }
                }


              }
            }
            // else if (pos === "bot") {
            //   if (winSize <= 719) {
            //     $('.sto-' + placeholder + '-vignette').detach();
            //     $('.fila4').eq(1).children('.span-5').eq(0).before(container);
            //
            //     $('.fila4').eq(1).children('.span-5').eq(4).detach().insertBefore('.fila4:nth-of-type(3) .span-5:first-of-type');
            //   } else {
            //     $('.sto-' + placeholder + '-vignette').detach();
            //     $('.fila4').eq(2).children('.span-5').eq(0).before(container);
            //
            //     for (var i = 0; i <= $('.fila4').length; i++) {
            //         if ($('.fila4').eq(i).children('div').length > 4) {
            //                 var curProd = $('.fila4').eq(i).children('div').eq(1);
            //
            //                 $('.fila4').eq(i - 1).children('.span-5').eq(2).after(curProd);
            //
            //         }
            //     }
            //   }
            // }
        };





        window.addEventListener("resize", function() {
            // Deal with window responsive
            reposition();

            largeurTablette();

            // height vignette
            vignetteHeight = $('.span-5').eq(1).height() - 10;
            $('.sto-' + placeholder + '-vignette').css('height', vignetteHeight + 'px');

        });
        //setInterval(reposition(), 100);

        window.setTimeout(function() {
            //window.dispatchEvent(new window.Event("resize"));
            var evt = window.document.createEvent('UIEvents');
            evt.initUIEvent('resize', true, false, window, 0);
            window.dispatchEvent(evt);

        });

        $(window).resize();

        var removers = {};
        removers[format] = function() {
            style.unuse();
            container.remove();
        };
        return removers;
        /*} else if($('.mode3').hasClass('selected')) {
          tracker.sendHit({"ta":"error","te":"onBuild","tz":"modeListOn"});
        }*/

    });
}

function getNbArticle() {
    var nbProducts = $(".vignette-box").length;
    if (nbProducts)
        return nbProducts;
    else
        return false;
}

"use strict";
var settings = require("./../../settings.json"),
  style = require("./main.css"),
  html = require("./main.html"),
  sto = window.__sto,
  format = settings.format,
  placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
  container = $(html),
  target = settings.target != "_blank" && settings.target != "_self" ? "_self" : settings.target,
  m_target = settings.mention_target != "_blank" && settings.mention_target != "_self" ? "_self" : settings.mention_target,
  pos = settings.ean == "" ? settings.position_format : "ean",
  m_txt = settings.mention_text,
  myurl = window.location.href,
  myurl = myurl.replace("https://", "").split("/")[1],
  big_line, line_half;

var roboto = $('<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');
$('head').append(roboto);

module.exports = {
  init: _init_()
}

function _init_() {
  sto.load(format, function(tracker) {
    tracker.display();
    style.use();

    switch (pos) {
      case 'middle':
        $('.c-product-thumbnails-container>.c-product-thumbnail').eq(4).before(container);
        break;
      case 'bottom':
        $('.c-product-thumbnails-container>.c-product-thumbnail').eq(8).before(container);
        break;
      case 'ean':
        $('.c-product-thumbnails-container>.c-product-thumbnail').eq(parseInt(settings.ean) - 1).before(container);
        big_line = Math.ceil(settings.ean / 4);
        if((settings.ean / 4).toString().split('.').length > 1){
          var decimal_half = parseInt(((settings.ean / 4).toString().split('.')[1]).charAt(0));
        } else {
          var decimal_half = 6;
        }
        if(decimal_half > 5 ){
          line_half = 1;
        } else {
          line_half = 2;
        }
        break;
      case 'top':
      default:
        $('.c-product-thumbnails-container>.c-product-thumbnail').eq(0).before(container);
        break;

    }

    if (m_txt && m_txt != "") {
        if (m_txt.length > 130) {
            m_txt = m_txt.slice(0, 130);
        }
        $(container).prepend($("<div class='sto-" + placeholder + "-legal'><span>" + m_txt + "</span></div>"));
        if(settings.mention_url != ""){
          $('.sto-' + placeholder + '-legal').attr('type','link');
          $('.sto-' + placeholder + '-legal').on('click', function(e) {
              e.preventDefault();
              e.stopPropagation();
              tracker.click({
                  "tl": "legal"
              });
              window.open(settings.mention_url, m_target);
          });
        }
    }

    /*if (settings.cta_img !== "") {
      // if(m_txt.length > 130){m_txt = m_txt.slice(0,130);}
      $(container).prepend($("<div class='sto-" + placeholder + "-cta'></div>"));
      $('.sto-' + placeholder + '-cta').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        tracker.sendHit({
          "ta": "clk"
        });
        window.open(settings.cta_url, settings.cta_target);
      });
    }*/

    $('.sto-' + placeholder + '-vignette').on('click', function(e) {
      e.preventDefault();
      tracker.click({
          "tl": "redirect"
      });
      window.open(settings.redirect, target);
    });

    //TRACKING VIEW
    var trackingView = false;
    var windowCenterTop = $(window).height() / 4;
    var windowCenterBot = $(window).height() - windowCenterTop;
    var eTop = $('.sto-' + placeholder + '-vignette').offset().top;
    var distanceToTop;
    var trackScroll = function() {
      distanceToTop = eTop - $(window).scrollTop();
      if (distanceToTop >= windowCenterTop && distanceToTop <= windowCenterBot && trackingView == false) {
        tracker.sendHit({
          "ta": "view",
        });
        trackingView = true;
      }
    };
    trackScroll();
    $(window).scroll(function() {
      trackScroll();
    });


    window.addEventListener("resize", function() {
      var size = $(window).width();
      if (pos === "middle") {
        if (size <= 973) {
          var resContainer = $('.sto-' + placeholder + '-vignette').detach();
          $('.c-product-thumbnails-container>.c-product-thumbnail').eq(2).before(container);
        } else if (size > 973) {
          var resContainer = $('.sto-' + placeholder + '-vignette').detach();
          $('.c-product-thumbnails-container>.c-product-thumbnail').eq(4).before(container);
        }
      } else if (pos === "bottom") {
        if (size <= 973) {
          var resContainer = $('.sto-' + placeholder + '-vignette').detach();
          $('.c-product-thumbnails-container>.c-product-thumbnail').eq(4).before(container);
        } else if (size > 973) {
          var resContainer = $('.sto-' + placeholder + '-vignette').detach();
          $('.c-product-thumbnails-container>.c-product-thumbnail').eq(8).before(container);
        }
      } else if (pos === "ean") {
        if (size <= 973) {
          var resContainer = $('.sto-' + placeholder + '-vignette').detach();
          $('.c-product-thumbnails-container>.c-product-thumbnail').eq((big_line * 2) - line_half).before(container);
        } else if (size > 973) {
          var resContainer = $('.sto-' + placeholder + '-vignette').detach();
          $('.c-product-thumbnails-container>.c-product-thumbnail').eq(parseInt(settings.ean) - 1).before(container);
        }
      }
    });
    window.setTimeout(function() {
      //window.dispatchEvent(new window.Event("resize"));
      var evt = window.document.createEvent('UIEvents');
      evt.initUIEvent('resize', true, false, window, 0);
      window.dispatchEvent(evt);
    });

    var removers = {};
    removers[format] = function() {
      style.unuse();
      container.remove();
    };
    return removers;
  });
}

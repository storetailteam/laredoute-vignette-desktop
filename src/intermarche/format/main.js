"use strict";
var settings = require("./../../settings.json"),
    style = require("./main.css"),
    html = require("./main.html"),
    sto = window.__sto,
    format = settings.format,
    placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
    container = $(html),
    redirect_url = settings.redirect,
    mention_url = settings.mention_url,
    cta_url = settings.cta_url,
    t_link = settings.target == "_blank" ? "_blank" : "_self",
    t_m_link = settings.mention_target == "_blank" ? "_blank" : "_self",
    t_cta = settings.cta_target == "_blank" ? "_blank" : "_self",
    pos = settings.position_format,
    m_txt = settings.mention_text,
    myurl = window.location.href,
    myurl = myurl.replace("https://", "").split("/")[1],
    index, page_type;

var roboto = $('<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');
$('head').append(roboto);

if (redirect_url || redirect_url != "") {
    if (redirect_url.indexOf('drive.intermarche.com') >= 0) {
        redirect_url = redirect_url.replace("https://drive.intermarche.com/", "");
        var url_redirection = "/" + myurl,
            y = 1;
        for (y; y < redirect_url.split("/").length; y++) {
            url_redirection = url_redirection + "/" + redirect_url.split("/")[y]
        }
    }
}

if (mention_url || mention_url != "") {
    if (mention_url.indexOf('drive.intermarche.com') >= 0) {
        mention_url = mention_url.replace("https://drive.intermarche.com/", "");
        var url_mention = "/" + myurl,
            y = 1;
        for (y; y < mention_url.split("/").length; y++) {
            url_mention = url_mention + "/" + mention_url.split("/")[y]
        }
    }
}

if (cta_url || cta_url != "") {
    if (cta_url.indexOf('drive.intermarche.com') >= 0) {
        cta_url = cta_url.replace("https://drive.intermarche.com/", "");
        var url_cta = "/" + myurl,
            y = 1;
        for (y; y < cta_url.split("/").length; y++) {
            url_cta = url_cta + "/" + cta_url.split("/")[y]
        }
    }
}

//js-vignette_rayon
switch (pos) {
    case 'top':
        index = 1;
        break;
    case 'middle':
        index = 5;
        break;
    case 'bottom':
        index = 9;
        break;
    case 'ean':
      setInterval(function() {

      }, 500);
        var cont = $(".content_vignettes").outerWidth();
        var vig = $('.content_vignettes .vignettes_produit .vignette_produit_info').width();
        var vignetteSize = parseInt(cont / vig);
        $('.content_vignettes .vignettes_produit .vignette_produit_info').each(function(i,v) {
          //console.log(settings.ean);
          if ($(v).find(".js-vignette_footer").attr("idproduit") == settings.ean) {
              var place = i+1;
              if (place % vignetteSize === 0) {
                $('.content_vignettes .vignettes_produit .vignette_produit_info').eq(place).insertBefore($('.content_vignettes .vignettes_produit .vignette_produit_info').eq(i));
                index = place+1;
              } else {
                index = place;
              }

          }
        });
        break;
    default:
        index = 1;
        break;
}

module.exports = {
    init: _init_()
}

function _init_() {
    sto.load(format, function(tracker) {
        tracker.display();
        style.use();

        $(container).insertBefore($('.content_vignettes .vignettes_produit .vignette_produit_info').eq(index - 1));
        respImp();

        if ($(document).find('.js-vignette_rayon').length != 0) {
            page_type = "rayon"
        } else {
            page_type = "recherche"
        }
        $(container).attr('data-page', page_type);

        if (m_txt && m_txt != "") {
            if (m_txt.length > 130) {
                m_txt = m_txt.slice(0, 130);
            }
            $(container).prepend($("<div class='sto-" + placeholder + "-legal'>" + m_txt + "</div>"));
            $('.sto-' + placeholder + '-legal').attr('type','link');
            $('.sto-' + placeholder + '-legal').on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();
                tracker.click();
                window.open(url_mention, t_m_link);
            });
        }

        if (settings.cta_img !== "") {
            $(container).prepend($("<div class='sto-" + placeholder + "-cta'></div>"));
            $('.sto-' + placeholder + '-cta').on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();
                tracker.click({
                    "tl": "legal"
                });
                window.open(url_cta, t_cta);
            });
        }

        $('.sto-' + placeholder + '-vignette').on('click', function(e) {
            e.preventDefault();
            tracker.click({
                "tl": "redirect"
            });
            window.open(url_redirection, t_link);
        });

        //TRACKING VIEW
        var trackingView = false;
        var windowCenterTop = $(window).height() / 4;
        var windowCenterBot = $(window).height() - windowCenterTop;
        var eTop = $('.sto-' + placeholder + '-vignette').offset().top;
        var distanceToTop;
        var trackScroll = function() {
            distanceToTop = eTop - $(window).scrollTop();
            if (distanceToTop >= windowCenterTop && distanceToTop <= windowCenterBot && trackingView == false) {
                tracker.view();
                trackingView = true;
            }
        };
        trackScroll();
        $(window).scroll(function() {
            trackScroll();
        });

        var removers = {};
        removers[format] = function() {
            style.unuse();
            container.remove();
        };
        return removers;
    });
}

function respImp() {
    $(window).resize(function() {
        var width = $(window).width(),
            newPlace = $(container).detach();

        if (width > 1279) {
            switch (pos) {
                case 'top':
                    index = 1;
                    break;
                case 'middle':
                    index = 5;
                    break;
                case 'bottom':
                    index = 9;
                    break;
                case 'ean':
                  setInterval(function() {

                  }, 500);
                    var cont = $(".content_vignettes").outerWidth();
                    var vig = $('.content_vignettes .vignettes_produit .vignette_produit_info').width();
                    var vignetteSize = parseInt(cont / vig);
                    $('.content_vignettes .vignettes_produit .vignette_produit_info').each(function(i,v) {
                      if ($(v).find(".js-vignette_footer").attr("idproduit") == settings.ean) {
                          var place = i+1;
                          if (place % vignetteSize === 0) {
                            $('.content_vignettes .vignettes_produit .vignette_produit_info').eq(place).insertBefore($('.content_vignettes .vignettes_produit .vignette_produit_info').eq(i));
                            index = place+1;
                          } else {
                            index = place;
                          }

                      }
                    });
                    break;
                default:
                    index = 1;
                    break;
            }
        } else if (width <= 1279) {
            switch (pos) {
                case 'top':
                    index = 1;
                    break;
                case 'middle':
                    index = 4;
                    break;
                case 'bottom':
                    index = 7;
                    break;
                case 'ean':
                  setInterval(function() {

                  }, 500);
                    var cont = $(".content_vignettes").outerWidth();
                    var vig = $('.content_vignettes .vignettes_produit .vignette_produit_info').width();
                    var vignetteSize = parseInt(cont / vig);
                    $('.content_vignettes .vignettes_produit .vignette_produit_info').each(function(i,v) {
                      if ($(v).find(".js-vignette_footer").attr("idproduit") == settings.ean) {
                          var place = i+1;
                          if (place % vignetteSize === 0) {
                            $('.content_vignettes .vignettes_produit .vignette_produit_info').eq(place).insertBefore($('.content_vignettes .vignettes_produit .vignette_produit_info').eq(i));
                            index = place;
                          } else {
                            index = place;
                          }

                      }
                    });
                    break;
                default:
                    index = 1;
                    break;
            }
        }
        $(newPlace).insertBefore($('.content_vignettes .vignettes_produit .vignette_produit_info').eq(index - 1));
    });

}

function getNbArticle() {
    var nbProducts = $(".vignette-box").length;
    if (nbProducts)
        return nbProducts;
    else
        return false;
}

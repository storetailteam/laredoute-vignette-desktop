"use strict";
var sto = window.__sto,
    $ = window.jQuery,
    settings = require("./../../settings.json"),
    style = require("./main.css"),
    html = require("./main.html"),
    format = settings.format,
    placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
    container = $(html),
    redirect = settings.redirect,
    mention_url = settings.mention_url,
    cta_url = settings.cta_url != "" ? settings.cta_url : redirect,
    pos = settings.position_format,
    m_txt = settings.mention_text,
    index, t_link, t_m_link, page_type;

    $(document).ajaxComplete(function(evt,xhr,set) { $(".item-page:last").children().appendTo(".item-page:first"); });

    var fontRoboto = $('<link href="//fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');
    $('head').first().append(fontRoboto);



module.exports = {
    init: _init_(),
    styleGutter: style
}

function _init_() {
    sto.load(format, function(tracker) {
        tracker.display();
        var container = $(html);

        style.use();

        switch (pos) {
            case "middle":
                index = 5;
                break;
            case "bottom":
                index = 10;
                break;
            case "top":
            default:
                index = 1;
                break;
        }

        $('article.item').eq(index - 1).before(container);

        if (!settings.target || settings.target != "_blank") { t_link = "_self";
        } else { t_link = "_blank";}

        if (!settings.mention_target || settings.mention_target != "_blank") { t_m_link = "_self";
        } else { t_m_link = "_blank";}



        if (!redirect || redirect == "") { redirect = "#";}
        else{ $(container).css({"cursor":"pointer"}); }

        if (!mention_url || mention_url == "") { mention_url = "#";}



        if (m_txt && m_txt != "") {
            if (m_txt.length > 130) {
                m_txt = m_txt.slice(0, 130);
            }


            $(container).prepend($("<div class='sto-" + placeholder + "-legal'>" + m_txt + "</div>"));

            if(mention_url != "#"){
                $(container).attr("mention","");
                $('.sto-' + placeholder + '-legal')
                    .attr('type','link')
                    .on('click', function(e) {
                        e.preventDefault();
                        e.stopPropagation();
                        tracker.click({
                            "tl": "legal"
                        });
                        window.open(mention_url, t_m_link);
                    });

                $('.sto-' + placeholder + '-redir_m').on('click', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    tracker.click({
                        "tl": "legal",
                        "tz": "mobile"
                    });
                    window.open(mention_url, t_m_link);
                });
            }
        }

        if (settings.cta_img !== "") {
            $(container).prepend($("<div class='sto-" + placeholder + "-cta'></div>"));
            $('.sto-' + placeholder + '-cta').on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();
                tracker.click();
                window.open(cta_url, settings.cta_target);
            });
        }

        if (redirect && redirect !== "") {
            $('.sto-' + placeholder + '-vignette').attr("redir","");
            $('.sto-' + placeholder + '-vignette').on('click', function(e) {
                e.preventDefault();
                tracker.click({
                    "tl": "redirect"
                });
                window.open(redirect, t_link);
            });
        }



        var listSize, btfDe, headerMobSize;
        window.addEventListener("resize", function() {

            listSize = $(window).outerWidth(true);

            setInterval(function(){
                if(listSize < 551){
                    $(container).css({"height":$(".item-page .item:not(.sto-vr-vignette)").eq(0).width() / 4.14 +"px"}).attr("mobile","");
                    $(".sto-" + placeholder + "-legal , .sto-" + placeholder + "-cta").css({"display":"none"});
                    var a = $(container).attr("mention");
                    if(typeof a !== typeof undefined && a !== false){
                        $(".sto-" + placeholder + "-redir_m").css({"display":"inline-block"});
                    }
                } else{
                    $(container).attr({"style":""}).removeAttr("mobile","");
                    $(".sto-" + placeholder + "-redir_m").css({"display":"none"});
                    $(".sto-" + placeholder + "-legal").css({"display":"inline-block"});
                    if (settings.cta_img !== "") {
                        $(".sto-" + placeholder + "-cta").css({"display":"inline-block"});
                    }
                }

            },100);

            if (pos == "middle" || pos == "bottom") {
                btfDe = $('.sto-' + placeholder + '-w-butterfly').detach();

                if (listSize < 551) {
                    switch (pos) {
                        case "middle":
                            index = 2;
                            break;
                        case "bottom":
                            index = 3;
                            break;
                    }
                } else if (listSize < 769) {
                    switch (pos) {
                        case "middle":
                            index = 3;
                            break;
                        case "bottom":
                            index = 5;
                            break;
                    }
                } else if (listSize < 1003) {
                    switch (pos) {
                        case "middle":
                            index = 4;
                            break;
                        case "bottom":
                            index = 7;
                            break;
                    }
                } else if (listSize >= 1003) {
                    switch (pos) {
                        case "middle":
                            index = 5;
                            break;
                        case "bottom":
                            index = 9;
                            break;
                    }
                }
                $('article.item').eq(index - 1).before(btfDe);

            }
        });

        window.setTimeout(function() {
            var evt = window.document.createEvent('UIEvents');
            evt.initUIEvent('resize', true, false, window, 0);
            window.dispatchEvent(evt);
        });

        var removers = {};
        removers[format] = function() {
            style.unuse();
            container.remove();
        };
        return removers;
    });
}

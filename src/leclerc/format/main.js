"use strict";
var settings = require("./../../settings.json"),
    style = require("./main.css"),
    html = require("./main.html"),
    sto = window.__sto,
    format = settings.format,
    placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
    container = $(html),
    pos = settings.position_format,
    m_txt = settings.mention_text,
    myurl = window.location.href,
    myurl = myurl.replace("https://", "").split("/")[1],
    index, t_link, t_m_link, page_type;



var roboto = $('<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');
$('head').append(roboto);

if (window.location.href.indexOf('recherche') > -1) {
    page_type = "recherche"
} else {
    page_type = "rayon"
}

if (!settings.target || settings.target != "_blank") {
    t_link = "_self";
} else {
    t_link = "_blank";
}
if (!settings.mention_target || settings.mention_target != "_blank") {
    t_m_link = "_self";
} else {
    t_m_link = "_blank";
}
if (!settings.redirect || settings.redirect == "") {
    settings.redirect = "";
}
if (!settings.mention_url || settings.mention_url == "") {
    settings.mention_url = "";
}
//js-vignette_rayon
switch (pos) {
    case 'top':
        index = 1;
        break;
    case 'mid':
        if (page_type === "rayon") {
            index = 6;
        } else {
            index = 5;
        }
        break;
    case 'bot':
        if (page_type === "rayon") {
            index = 10;
        } else {
            index = 9;
        }
        break;
    default:
        index = 1;
        break;
}

module.exports = {
    init: _init_()
}




function _init_() {
    sto.load(format, function(tracker) {

        tracker.display();
        style.use();


        var emplacement = $('#ulListeProduits .liWCRS310_Product').eq(index - 1);
        //$(container).insertBefore(emplacement);
        $("#divWCRS310_ProductsList").prepend(container);

        if (pos == "mid") {
            $('.sto-' + placeholder + '-vignette').attr('pos', 'mid')
        }
        if (pos == "bot") {
            $('.sto-' + placeholder + '-vignette').attr('pos', 'bot')
        }


        $(container).attr('data-page', page_type);

        if (m_txt && m_txt != "") {
            if (m_txt.length > 130) {
                m_txt = m_txt.slice(0, 130);
            }
            $(container).prepend($("<div class='sto-" + placeholder + "-legal'>" + m_txt + "</div>"));
            if (settings.mention_url != "#") {
                $('.sto-' + placeholder + '-legal').attr('type', 'link');
                $('.sto-' + placeholder + '-legal').on('click', function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    tracker.click({
                        "tl": "legal"
                    });
                    window.open(settings.mention_url.replace("_DRIVE_", myurl), t_m_link);
                });
            }
        }

        $('.sto-' + placeholder + '-vignette').on('click', function(e) {
            e.preventDefault();
            tracker.click({
                "tl": "redirect"
            });
            window.open(settings.redirect.replace("_DRIVE_", myurl), t_link);
        });

        //TRACKING VIEW
        var trackingView = false;
        var windowCenterTop = $(window).height() / 4;
        var windowCenterBot = $(window).height() - windowCenterTop;
        var eTop = $('.sto-' + placeholder + '-vignette').offset().top;
        var distanceToTop;
        var trackScroll = function() {
            distanceToTop = eTop - $(window).scrollTop();
            if (distanceToTop >= windowCenterTop && distanceToTop <= windowCenterBot && trackingView == false) {
                tracker.view();
                trackingView = true;
            }
        };
        trackScroll();
        $(window).scroll(function() {
            trackScroll();
        });




        // resize for pos mid

        var tgSize = 0;
        var winSize;
        if ($('.liWMKP001_TeteGondole').length > 0) {
            var tgSize = $('.liWMKP001_TeteGondole>ul').children().length - 1;
        }

        var reposition = function(p,q) {
            winSize = $('#divContenuCentre').width();
            //console.log(winSize);
            if (winSize <= 597) {
                if (driveVersion === "http") {
                    if (tgSize >= 3) {
                        $('.liWMKP001_TeteGondole').css('margin-left', '0px');
                        $(".liWCRS310_Product").css('margin-left', '0px');
                        $('.liWMKP001_TeteGondole').siblings('li.liWCRS310_Product').first().css({
                            "margin-left": "199px"
                        });
                        if(pos == "bot"){
                          $('.liWMKP001_TeteGondole').siblings('li.liWCRS310_Product').first().css({
                              "margin-left": "0px"
                          });
                          $(".liWCRS310_Product:nth-child(" + (4 - tgSize) + ")").css({
                              "margin-left": "199px"
                          });
                        }
                    } else {
                        $('.liWMKP001_TeteGondole').css('margin-left', '0px');
                        $(".liWCRS310_Product").css('margin-left', '0px');
                        $(".liWCRS310_Product:nth-child(" + (((4 * p) - q) - tgSize) + ")").css({
                            "margin-left": "199px"
                        });
                    }
                }
            } else if (winSize < 974) {
                if (tgSize >= 3) {
                    $('.liWMKP001_TeteGondole').css('margin-left', '0px');
                    $(".liWCRS310_Product").css('margin-left', '0px');
                    $('.liWMKP001_TeteGondole').siblings('li.liWCRS310_Product').first().css({
                        "margin-left": "199px"
                    });
                    if(pos == "bot"){
                      $('.liWMKP001_TeteGondole').siblings('li.liWCRS310_Product').first().css({
                          "margin-left": "0px"
                      });
                      $(".liWCRS310_Product:nth-child(" + (5 - tgSize) + ")").css({
                          "margin-left": "199px"
                      });
                    }
                } else {
                    $('.liWMKP001_TeteGondole').css('margin-left', '0px');
                    $(".liWCRS310_Product").css('margin-left', '0px');
                    $(".liWCRS310_Product:nth-child(" + (((5 * p) - q) - tgSize) + ")").css({
                        "margin-left": "199px"
                    });
                }
            } else if (winSize < 995) {
                if (tgSize < 4 && tgSize > 0) {
                    $('.liWMKP001_TeteGondole').css('margin-left', '0px');
                    $(".liWCRS310_Product").css('margin-left', '0px');
                    $('.liWMKP001_TeteGondole').siblings('li.liWCRS310_Product').first().css({
                        "margin-left": "199px"
                    });
                    if(pos == "bot"){
                      $('.liWMKP001_TeteGondole').siblings('li.liWCRS310_Product').first().css({
                          "margin-left": "0px"
                      });
                      $(".liWCRS310_Product:nth-child(" + (5 - tgSize) + ")").css({
                          "margin-left": "199px"
                      });
                    }
                } else {
                    $('.liWMKP001_TeteGondole').css('margin-left', '0px');
                    $(".liWCRS310_Product").css('margin-left', '0px');
                    $(".liWCRS310_Product:nth-child(" + (((5 * p) - q) - tgSize) + ")").css({
                        "margin-left": "199px"
                    });
                }
            } else if (winSize < 1194) {
                $('.liWMKP001_TeteGondole').css('margin-left', '0px');
                $(".liWCRS310_Product").css('margin-left', '0px');
                $(".liWCRS310_Product:nth-child(" + (((6 * p) - q) - tgSize) + ")").css({
                    "margin-left": "199px"
                });
            } else if (winSize < 1393) {
                $('.liWMKP001_TeteGondole').css('margin-left', '0px');
                $(".liWCRS310_Product").css('margin-left', '0px');
                if ($('body.bodyExtensible').length < 1) {
                    $(".liWCRS310_Product:nth-child(" + (((6 * p) - q) - tgSize) + ")").css({
                        "margin-left": "199px"
                    });
                } else {
                    $('.liWMKP001_TeteGondole').css('margin-left', '0px');
                    $(".liWCRS310_Product").css('margin-left', '0px');
                    $(".liWCRS310_Product:nth-child(" + (((7 * p) - q) - tgSize) + ")").css({
                        "margin-left": "199px"
                    });
                }

            } else if (winSize >= 1393) {
                if ($('body.bodyExtensible').length < 1) {
                    $('.liWMKP001_TeteGondole').css('margin-left', '0px');
                    $(".liWCRS310_Product").css('margin-left', '0px');
                    $(".liWCRS310_Product:nth-child(" + (((6 * p) - q) - tgSize) + ")").css({
                        "margin-left": "199px"
                    });
                } else {
                    $('.liWMKP001_TeteGondole').css('margin-left', '0px');
                    $(".liWCRS310_Product").css('margin-left', '0px');
                    $(".liWCRS310_Product:nth-child(" + (((8 * p) - q) - tgSize) + ")").css({
                        "margin-left": "199px"
                    });
                }

            }
        };


        window.addEventListener("resize", function() {
            // Deal with window responsive
            if (pos == "mid") {
                reposition(1,0);
            }else if(pos == "bot"){
                reposition(2,1);
            }
        });
        //setInterval(reposition(), 100);

        window.setTimeout(function() {
            //window.dispatchEvent(new window.Event("resize"));
            var evt = window.document.createEvent('UIEvents');
            evt.initUIEvent('resize', true, false, window, 0);
            window.dispatchEvent(evt);

        });

        $(window).resize();

        var removers = {};
        removers[format] = function() {
            style.unuse();
            container.remove();
        };
        return removers;
        /*} else if($('.mode3').hasClass('selected')) {
          tracker.sendHit({"ta":"error","te":"onBuild","tz":"modeListOn"});
        }*/

    });
}

function getNbArticle() {
    var nbProducts = $(".vignette-box").length;
    if (nbProducts)
        return nbProducts;
    else
        return false;
}

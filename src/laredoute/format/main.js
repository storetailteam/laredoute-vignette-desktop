"use strict";

var settings = require("./../../settings.json"),
  style = require("./main.css"),
  html = require("./main.html"),
  sto = window.__sto,
  format = settings.format,
  placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
  creaid = settings.creaid,
  container = $(html),
  target = settings.cta_target != "_blank" && settings.target != "_self" ? "_self" : settings.target,
  m_target = settings.mention_target != "_blank" && settings.mention_target != "_self" ? "_self" : settings.mention_target,
  pos,
  m_txt = settings.mention_text,
  cta_txt = settings.cta_text,
  left_txt = settings.left_text,
  left_cta = settings.left_cta_text,
  // left_cta_redirect = settings.left_cta_redirect != "_blank" && settings.left_cta_redirect != "_self" ? "_self" : settings.left_cta_redirect,
  myurl = window.location.href,
  myurl = myurl.replace("https://", "").split("/")[1],
  big_line, line_half, posGlobals;

  $('#csp_module_plp').hide();
  $('#trade_ccd_module_plp_1').hide();
  $('#trade_ccd_module_plp_2').hide();
  $('#trade_ccd_module_plp_3').hide();
  $('#csp_module_plp_search').hide();
  $('#trade_ccd_module_plp_search_1').hide();
  $('#trade_ccd_module_plp_search_2').hide();
  $('#trade_ccd_module_plp_search_3').hide();

var roboto = $('<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">');
$('head').append(roboto);

module.exports = {
  init: _init_()
}

function _init_() {

  if (window.__sto.getTrackers("VI").constructor === Array) {
    var cosa = [format, creaid];
  } else {
    var cosa = format;
  }

  sto.load([format, creaid], function (tracker) {

    var removers = {};
    removers[format] = {
      "creaid": creaid,
      "func": function () {
        style.unuse();
        container.remove(); //verifier containerFormat j'ai fait un copier coller
      }
    };


    if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 3) {
      return removers;
    }


    var int, isElementInViewport;

    isElementInViewport = function (el) {

      if (typeof window.jQuery === "function" && el instanceof window.jQuery) {
        el = el[0];
      }


      var rect = el.getBoundingClientRect();

      return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        !(rect.top == rect.bottom || rect.left == rect.right) &&
        !(rect.height == 0 || rect.width == 0) &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
        rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
      );

    }

    // if (window.matchMedia('(min-width: 786px)').matches) {
    //   tracker.error({
    //     "tl": "onlyOnMobile"
    //   });
    //   return removers;
    //   return false;
    // }



    /*if ($('div[data-pos="' + settings.position_format + '"]').length > 0) {
      tracker.error({
        "tl": "placementOcuppied"
      });
      return removers;
      return false;
    }*/

    // if ($('div[data-pos="7"]').length > 0 && $('div[data-pos="17"]').length > 0) {
    //   tracker.error({
    //     "tl": "placementOcuppied"
    //   });
    //   return removers;
    //   return false;
    // }


    // var prodsInpage = $('#productList>li').length;
    //
    // if (prodsInpage >= 14 && prodsInpage <= 20 && $('.sto-' + format).length < 1) {
    //
    // } else if (prodsInpage > 20 && $('.sto-' + format).length < 2) {
    //
    // } else {
    //   tracker.error({
    //     "tl": "notEnoughProdsInPage"
    //   });
    //   return removers;
    //   return false;
    // }
    //
    // var prodsInpage = $('#productList>li').length;
    //
    // if (prodsInpage >= 14 && prodsInpage <= 20 && $('.sto-' + format).length < 1) {
    //   tracker.display({
    //     "po": pos
    //   });
    // } else if (prodsInpage > 20 && $('.sto-' + format).length < 4) {
    //   tracker.display({
    //     "po": pos
    //   });
    // }
    // else {
    //   tracker.error({
    //     "tl": "notEnoughProdsInPage"
    //   });
    //   return removers;
    //   return false;
    // }

    style.use();

    // container.attr('data-pos', settings.position_format);
    container.attr('data-format-type', settings.format);
    container.attr('data-crea-id', settings.creaid);

    /*$('#productList>li.product').eq(parseInt(settings.position_format) - 1).before(container);*/

    // <editor-fold> INSERT CONTAINER *****************************************************


    //insert format
    var insertFormat = function () {

      var widthTile = $('#productList>li').width();
      var widthLine = $('.product-list').width();
      var nbTile;

      if (widthTile * 2 > widthLine) {
        nbTile = 1;
      } else if (widthTile * 3 > widthLine) {
        nbTile = 2;
      } else {
        nbTile = 3;
      }

      var sponso = $('.hl-beacon-universal').length;
      if (sponso >= 1) {
        sponso = 1;
      }

      var sponsoTech = $('.breakzone').length;
      if (sponsoTech >= 1) {
        sponsoTech = 1;
      }
      // if ($('.sto-format').length == 0 && sponsoTech >= 1 && nbTile == 1) {
      //   sponsoTech = 5;
      // } else if ($('.sto-format').length == 1 && sponsoTech >= 1 && nbTile == 1) {
      //   sponsoTech = 1;
      // } else if ($('.sto-format').length == 2 && sponsoTech >= 1 && nbTile == 1) {
      //   sponsoTech = -3;
      // } else {
      //   sponsoTech = 0;
      // }

      if (window.matchMedia("(max-width: 767px)").matches && nbTile == 1) {
        if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 0) {
          $('#productList>li:visible').eq(nbTile * 5).before(container);
          container.attr('data-pos', 6);
          posGlobals = 6;
        } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 1) {
          $('#productList>li:visible').eq(nbTile * 14).before(container);
          container.attr('data-pos', 16);
          posGlobals = 16;
        } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 2) {
          $('#productList>li:visible').eq(nbTile * 23).before(container);
          container.attr('data-pos', 26);
          posGlobals = 26
        }
      } else if (window.matchMedia("(min-width: 768px)").matches && nbTile == 1) {
        if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 0) {
          $('#productList>li:visible').eq(nbTile * (11 + sponsoTech)).before(container);
          container.attr('data-pos', 12 + sponsoTech);
          posGlobals = 12;
        } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 1) {
          $('#productList>li:visible').eq(nbTile * (16 + sponsoTech)).before(container);
          container.attr('data-pos', 18 + sponsoTech);
          posGlobals = 18;
        } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 2) {
          $('#productList>li:visible').eq(nbTile * (21 + sponsoTech)).before(container);
          container.attr('data-pos', 24 + sponsoTech);
          posGlobals = 24;
        }
      } else {
        if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 0) {
          $('#productList>li:visible').eq(nbTile * (5 + sponso)).before(container);
          container.attr('data-pos', 6 + sponso);
          posGlobals = 6;
        } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 1) {
          $('#productList>li:visible').eq(nbTile * (14 + sponso)).before(container);
          container.attr('data-pos', 16 + sponso);
          posGlobals = 16;
        } else if ($('.sto-format:not(.sto-AMSK, .sto-AFOO)').length == 2) {
          $('#productList>li:visible').eq(nbTile * (23 + sponso)).before(container);
          container.attr('data-pos', 26 + sponso);
          posGlobals = 26
        }
      }
    }

    insertFormat();

    pos = $('.sto-' + placeholder + '-vignette').closest('.sto-format').attr('data-pos');


    // </editor-fold> *********************************************************************

    var prodsInpage = $('#productList>li').length;

    if (prodsInpage >= 14 && prodsInpage <= 20 && $('.sto-' + format).length < 1) {
      // tracker.display({
      //   "po": pos
      // });
    } else if (prodsInpage > 20 && $('.sto-' + format).length < 4) {
      // tracker.display({
      //   "po": pos
      // });
    } else {
      tracker.error({
        "tl": "notEnoughProdsInPage"
      });
      return removers;
      return false;
    }
    
    if ($('.sto-' + placeholder + '-vignette').length === 1) {
      tracker.display({
        "po": pos
      });
    }




    // <editor-fold> LEFT *****************************************************************

    // TEXTE
    if (left_txt && left_txt != "") {
      $(container).find('.sto-' + placeholder + '-vignette-left .sto-' + placeholder + '-left-content').prepend($("<p class='sto-" + placeholder + "-left-txt'>" + left_txt + "</p>"));
    }

    // CTA
    // if (left_cta && left_cta != "") {
    //   if (left_cta.length > 130) {
    //     left_cta = left_cta.slice(0, 130);
    //   }
    // $(container).find('.sto-' + placeholder + '-vignette-left .sto-' + placeholder + '-left-content').append($("<a class='sto-" + placeholder + "-left-cta'>" + left_cta + "</a>"));
    $(container).find('.sto-' + placeholder + '-vignette-left .sto-' + placeholder + '-left-content').append($("<div class='sto-" + placeholder + "-left-arrow'></div>"));
    //   if (settings.left_cta_redirect != "") {
    //     $('.sto-' + placeholder + '-left-cta').attr('type', 'link');
    //     $('.sto-' + placeholder + '-left-cta').on('click', function(e) {
    //       e.preventDefault();
    //       e.stopPropagation();
    //       tracker.click({
    //         "tl": "CTA",
    //         "po": pos
    //       });
    //       window.open(settings.left_cta_redirect);
    //     });
    //   }
    // }



    // </editor-fold> *********************************************************************


    // <editor-fold> RESIZE ***************************************************************
    $(window).resize(function () {

      var resizeFormat = function () {
        var widthTile = $('#productList>li').width();
        var widthLine = $('.product-list').width();
        var nbTile;

        if (widthTile * 2 > widthLine) {
          nbTile = 1;
        } else if (widthTile * 3 > widthLine) {
          nbTile = 2;
        } else {
          nbTile = 3;
        }

        $('.sto-format').each(function () {
          var dataPos = $(this).attr('data-pos');

          // if (window.matchMedia("(max-width: 767px)").matches) {

          if (nbTile == 1) {
            if (window.matchMedia("(max-width: 767px)").matches) {
              if (dataPos >= 6 && dataPos <= 13) {
                $('#productList>li:visible').eq(nbTile * 5).before(this);
              } else if (dataPos >= 16 && dataPos <= 20) {
                $('#productList>li:visible').eq(nbTile * 14).before(this);
              } else if (dataPos >= 22 && dataPos <= 28) {
                $('#productList>li:visible').eq(nbTile * 23).before(this);
              }
            }
            if (window.matchMedia("(min-width: 768px)").matches) {
              if (dataPos >= 6 && dataPos <= 13) {
                $('#productList>li:visible').eq(nbTile * (dataPos - 1)).before(this);
              } else if (dataPos >= 16 && dataPos <= 20) {
                $('#productList>li:visible').eq(nbTile * (dataPos - 2)).before(this);
              } else if (dataPos >= 22 && dataPos <= 28) {
                $('#productList>li:visible').eq(nbTile * (dataPos - 3)).before(this);
              }
            }
          } else if (nbTile > 1) {
            if (dataPos >= 6 && dataPos <= 13) {
              $('#productList>li:visible').eq(nbTile * (dataPos - 1)).before(this);
            } else if (dataPos >= 16 && dataPos <= 17) {
              $('#productList>li:visible').eq(nbTile * (dataPos - 2)).before(this);
            } else if (dataPos >= 23 && dataPos <= 27) {
              $('#productList>li:visible').eq(nbTile * (dataPos - 3)).before(this);
            }
          }
        });
      }
      resizeFormat();

    });


    // </editor-fold> *********************************************************************




    //VIEW

    // int = window.setInterval(function() {
    //
    //   var dataPos = $('.sto-' + placeholder + '-vignette').closest('.sto-format').attr('data-pos');
    //
    //   if (isElementInViewport($('.sto-format[data-pos= dataPos]'))) {
    //
    //     tracker.view({
    //       "tl": 'hello'
    //     });
    //
    //
    //     window.clearInterval(int);
    //   }
    //
    // }, 200);



    // <editor-fold> TEXTE VIGNETTE *******************************************************
    if (m_txt && m_txt != "") {
      if (m_txt.length > 130) {
        m_txt = m_txt.slice(0, 130);
      }
      $(container).find('.sto-' + placeholder + '-vignette').prepend($("<div class='sto-" + placeholder + "-legal'><span>" + m_txt + "</span></div>"));
    }
    // </editor-fold> *********************************************************************


    // $('.sto-' + placeholder + '-vignette>a').attr('href',settings.redirect);
    // $('.sto-' + placeholder + '-vignette>a').attr('target',settings.target);
    // $('.sto-' + placeholder + '-vignette>a').on('click', function (e) {
    // tracker.click({
    //   "tl": "redirect",
    //   "po": pos
    // });
    // window.setTimeout(function(){
    //   /*if (settings.target == '_self') {
    //     window.location.href = settings.redirect;
    //   }*/
    //
    // }, 200)




    if (settings.redirect != "") {
      // $('.sto-' + placeholder + '-vignette').closest('.sto-format').find('a').attr('href',settings.redirect);
      // $('.sto-' + placeholder + '-vignette').closest('.sto-format').find('a').attr('href',settings.redirect);
      $(document).on('click', '.sto-' + placeholder + '-vignette', function (e) {
        e.preventDefault();
        e.stopPropagation();
        tracker.click({
          "tl": "redirect",
          "po": pos
        });

        window.open(settings.redirect, settings.target);

      });
      // $('.sto-' + format + '>a').attr('href',settings.redirect);
      // $('.sto-' + format + '>a').attr('target',settings.target);
      // $('.sto-' + format + '>a').on('click', function (e) {
      //
      //   e.preventDefault();
      //   e.stopPropagation();
      //   tracker.click({
      //     "tl": "redirect",
      //     "po": pos
      //   });
      //   window.open(settings.redirect, settings.target);
      // });
    }


    //TRACKING VIEW


    var trackingView = false;
    var windowCenterTop = $(window).height() / 4;
    var windowCenterBot = $(window).height() - windowCenterTop;
    var eTop = $('.sto-' + placeholder + '-vignette').offset().top;

    var dataPosition = $('.sto-' + placeholder + '-vignette').closest('.sto-format').attr('data-pos');

    var distanceToTop;
    var trackScroll = function () {
      distanceToTop = eTop - $(window).scrollTop();
      if (distanceToTop >= windowCenterTop && distanceToTop <= windowCenterBot && trackingView == false) {
        tracker.view({
          "po": dataPosition
          // "po": pos
        });
        trackingView = true;
      }
    };
    trackScroll();


    $(window).scroll(function () {
      trackScroll();
    });


    window.addEventListener("resize", function () {

      if (window.matchMedia("(max-width: 991px)").matches) {
        $('.sto-' + format).css('height', ((77.3 * $('.sto-' + placeholder + '-vignette').width()) / 100) + 'px');
      }

    });
    window.setTimeout(function () {
      //window.dispatchEvent(new window.Event("resize"));
      var evt = window.document.createEvent('UIEvents');
      evt.initUIEvent('resize', true, false, window, 0);
      window.dispatchEvent(evt);
    });


    //STORE FORMAT FOR FILTERS
    sto.globals["print_creatives"].push({
      html: container,
      parent_container_id: "#productList",
      type: settings.format,
      crea_id: settings.creaid,
      position: posGlobals
    });

    return removers;
  });
}

"use strict";
var settings = require("./../../settings.json"),
  style = require("./main.css"),
  html = require("./main.html"),
  sto = window.__sto,
  format = settings.format,
  placeholder = `${settings.format}_${settings.name}_${settings.creaid}`,
  container = html,
  pos = settings.position_format,
  m_txt = settings.mention_text,
  index, ligne, t_link, t_m_link;

if (!settings.target || settings.target != "_blank") {
  t_link = "_self";
} else {
  t_link = "_blank";
}
if (!settings.mention_target || settings.mention_target != "_blank") {
  t_m_link = "_self";
} else {
  t_m_link = "_blank";
}
if (!settings.redirect || settings.redirect == "") {
  settings.redirect = "#";
}
if (!settings.mention_url || settings.mention_url == "") {
  settings.mention_url = "#";
}

switch (pos) {
  case 'top':
    index = 1;
    break;
  case 'middle':
    index = 12;
    break;
  case 'bottom':
    index = 14;
    break;
  default:
    index = 1;
    break;
}

var s = document.querySelectorAll('.sto-' + placeholder + '-vignette')[0];
module.exports = {
  init: _init_(),
  styleGutter: style
}

function _init_() {
  sto.load(format, function(tracker) {
    tracker.display();
    style.use();
    document.querySelectorAll('.product-list--container.grid article')[index - 1].insertAdjacentHTML('beforebegin', container);
    generateVignette(m_txt, tracker);


    /*$('.sto-'+placeholder+'-cta').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        tracker.sendHit({
            "ta": "clk",
            "te": "voir mention - PDF"
        });
        window.open(settings.mention_url.replace("_DRIVE_", myurl), t_m_link);
    });*/

    // Deal with window size
    window.addEventListener("resize", function() {
      var width = window.innerWidth;
      if (width > 1919) {
        switch (pos) {
          case 'top':
            index = 1;
            break;
          case 'middle':
            index = 8;
            break;
          case 'bottom':
            index = 14;
            break;
          default:
            index = 1;
            break;
        }
      } else if (width <= 1919 && width > 1439) {
        switch (pos) {
          case 'top':
            index = 1;
            break;
          case 'middle':
            index = 7;
            break;
          case 'bottom':
            index = 12;
            break;
          default:
            index = 1;
            break;
        }
      } else if (width <= 1439 && width > 1023) {
        switch (pos) {
          case 'top':
            index = 1;
            break;
          case 'middle':
            index = 6;
            break;
          case 'bottom':
            index = 10;
            break;
          default:
            index = 1;
            break;
        }
      } else if (width <= 1023 && width > 767) {
        switch (pos) {
          case 'top':
            index = 1;
            break;
          case 'middle':
            index = 5;
            break;
          case 'bottom':
            index = 8;
            break;
          default:
            index = 1;
            break;
        }
      } else if (width <= 767) {
        switch (pos) {
          case 'top':
            index = 1;
            break;
          case 'middle':
            index = 4;
            break;
          case 'bottom':
            index = 6;
            break;
          default:
            index = 1;
            break;
        }
      }


      document.querySelectorAll('.sto-' + placeholder + '-vignette')[0].remove();
      document.querySelectorAll('.product-list--container.grid article')[index - 1].insertAdjacentHTML('beforebegin', container);
      generateVignette(m_txt, tracker);


      var int;
      int = window.setInterval(function() {
        if (isElementInViewport(document.querySelectorAll('.sto-' + placeholder + '-vignette')[0])) {
          tracker.view({
            "tl": "ViewVRedir",
            "tz": "vignetteRedirection"
          });
          window.clearInterval(int);
        }
      }, 200);




    });
    window.setTimeout(function() {
      //window.dispatchEvent(new window.Event("resize"));
      var evt = window.document.createEvent('UIEvents');
      evt.initUIEvent('resize', true, false, window, 0);
      window.dispatchEvent(evt);
    });

    var removers = {};
    removers[format] = function() {
      style.unuse();
      container.remove();
    };
    return removers;
  });
}

function isElementInViewport(el) {
  if (typeof window.jQuery === "function" && el instanceof window.jQuery) {
    el = el[0];
  }

  var rect = el.getBoundingClientRect();
  return (
    rect.top >= 0 &&
    rect.left >= 0 &&
    !(rect.top == rect.bottom || rect.left == rect.right) &&
    !(rect.height == 0 || rect.width == 0) &&
    rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
    rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
  );
}

function getNbArticle() {
  var nbProducts = $('.product-list--container article').length;
  if (nbProducts)
    return nbProducts;
  else
    return false;
}

function deleteVignette() {

}

function generateVignette(m_txt, tracker) {

  if (m_txt && m_txt != "") {
    if (m_txt.length > 130) {
      m_txt = m_txt.slice(0, 130);
    }
    document.querySelectorAll('.sto-' + placeholder + '-vignette')[0].insertAdjacentHTML('beforeend', "<label class='sto-" + placeholder + "-cta ui-btn default m red'><span>" + m_txt + "</span></label>");
    //$(container).prepend($("<div class='sto-"+placeholder+"-cta'><span>"+m_txt+"</span></div>"));
  }else{
    document.querySelectorAll('.sto-' + placeholder + '-vignette')[0].insertAdjacentHTML('beforeend', "<label class='sto-" + placeholder + "-cta ui-btn default m red'><span></span></label>");
  }

  document.querySelector('.sto-' + placeholder + '-vignette').addEventListener('click', function(e) {
    e.preventDefault();
    tracker.click();
    window.open(settings.redirect, t_link);
  });
}
